export type user = {
  userId: string;
  userName: string;
  userNameFull: string;
  sex: string | null;
  phone: string | null;
  phoneOffice: string | null;
  email: string;
  defaultDeptId: string;
  defaultDeptName: string;
  defaultOrgId: string;
  defaultOrgName: string;
  deptInfoList: object[];
}