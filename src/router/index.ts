import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Comment",
    component: () => import("../views/test.vue"),
    children: [
      // {
      //   path: "/template",
      //   component: () => import("../views/template/index.vue"),
      // },
    ]
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;