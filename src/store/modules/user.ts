import { Module } from "vuex"
import { State } from "..";
import { user } from "../../../typescript"


const initialState = {
    user: {} as user
}

// 类型推断
export type UserState = typeof initialState;

export default {
    namespaced: true,
    state: initialState,
    mutations: {
        updateUser(state, payload: user) {
            state.user = payload;
        }
    },
    actions: {
        updateUser({ commit }, params) {
            commit('updateUser', params)
        }
    }
} as Module<UserState, State>