import { InjectionKey } from 'vue'
import { createStore, Store } from 'vuex';
import persistedState from 'vuex-persistedstate'
import user, { UserState } from './modules/user'

// 创建 inhectionkey
export const key:InjectionKey<Store<State>> = Symbol();

export type State = {
    userModules?:UserState
}


export default createStore({
    state: {},
    modules: {
        userModules:user
    },
    plugins: [persistedState({ storage: window.sessionStorage })]
})